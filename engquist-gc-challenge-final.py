# GC Challenge script by Eric Engquist
# eric.engquist@gmail.com
# February 28, 2018

"""

*** GC Challenge Problem Statement ***

The monitor script needs to:

Parse GC log file. Since GC will rotate logs (say, once a day), script 
should switch between files to parse the current logs.

If one of the two conditions below are met, restart Tomcat running on 
that server and alert

  1) During a 5 min window, count the number of FullGC events in which 
     the memory being freed is below 20% (that is, memory being freed 
     is very low).  If count > 10

  2) If total FullGC time during a 5min window is greater than 15%

You need to ensure that before you reboot Tomcat on a node, you check if
all the other nodes in the cluster are up and responsive to not cause an 
outage to our users.

Lastly, the script should send GC metrics in real time to New Relic using 
the New Relic Insights API.

If you don't have an account in New Relic and don't want to create a new 
one, you can just log the request JSON that would be sent to New Relic. 
The important here is to see how you will implement this.

All GC events described below should be sent to New Relic Insights

"""

from datetime import datetime, timedelta
from time import mktime
from subprocess import call, Popen, PIPE
from re import finditer
from email.mime.text import MIMEText

####################
# Script variables #
####################

# VALUES FOR IDENTIFYING TARGET LOG FILES
#
# Full log file path is built from gc_log_path + gc_log_file_name + 
# <datestamp built in log_file_location()> + gc_log_file_extension
#
# GC log path
gc_log_path = "./logs/"
#
# GC log base file name
gc_log_file_name = "gc-logs-"
#
# GC log file extension
gc_log_file_extension = ".log"

# VALUES FOR GC TEST SENSITIVITY
#
# Adjust to make tests more/less sensitive or to change alert thresholds
#
# Threshold for low memory reclamation by full GC (post GC/pre GC memory)
# For example, 0.2 makes any full GC reclaiming less that 20% memory "bad"
full_gc_low_mem_threshold = 0.2
#
# Alert if this many or more low memory full GC events are detected
max_low_mem_full_GCs = 10
#
# Alert if full GCs occupied at least this portion of the monitored time period
max_full_GC_time = 0.15
#
# Time period (seconds) over which GC logs are evaluated.  This is used for
# mathematical calculations later in the script.  It does not 
# automatically change how frequently the script is run.  Assumes seconds.
log_time_period = 300 # 5 minutes = 300 seconds

# AWS VALUES
#
# ARN of load balancer target group used to evaluate tomcat server status
load-balancer-arn = arn:aws:elasticloadbalancing:us-east-1:119283105914:targetgroup/tomcat-target-group-green/08a0e0ed4de21ab8 # ... or whatever it is
#
# Expected host count (tomcat will not be restarted unless this many
# hosts appear to be healthy)
expected_tomcat_host_count = 4 


# Return full file name and path for GC log being evaluated
def log_file_location():

  # Assume only the current day's log file is being examined
  d = datetime.now()

  # Modify the below line to suit timestamps used by GC files
  log_file_datestamp = "%s-%s-%s" % (d.strftime('%Y'), d.strftime('%m'), d.strftime('%d'))

  # Build list of file path/name components and collapse into final string
  target_log_path = []
  target_log_path.append(gc_log_path)
  target_log_path.append(gc_log_file_name)
  target_log_path.append(log_file_datestamp)
  target_log_path.append(gc_log_file_extension)
  return "".join(target_log_path)

def get_last_five_minutes_of_gc_logs(logfile):

  # Find approximate index of logs younger than five minutes
  time_index = 1 # Start with first (most recently written) line
  
  index_found = False # set to true when we find our rough index

  with open(logfile) as f:
    all_lines = f.readlines() # memory issues for extremely large log files?

    # Identify time before which logs are too old to evaluate
    # Use log_time_period in seconds divided by 60 for minutes to review
    cutoff_time = datetime.now() - timedelta(minutes = log_time_period/60)

    # While there are logs to consume and we haven't found our rough index...
    while time_index < len(all_lines) and not index_found:

      # Find timestamp of log time_index lines back from end of file
      log_datetime = get_log_time(all_lines[len(all_lines)-time_index])
      
      # If that timestamp is after our cutoff time, double time_index
      # and try again
      if log_datetime > cutoff_time:
        time_index = time_index * 2
      else: # Otherwise, stop searching.  We have our rough index.
        index_found = True

  # Now we know roughly where to start scanning logs
  logs_to_evaluate = all_lines[-1*(min(len(all_lines), time_index)):]

  # We got our rough chunk of logs to evaluate
  # Now, prune the early entries that were unintentionally added via
  # exponential exploration of log file
  extra_logs_pruned = False
  
  # For each line, pop if timestamp is too old.
  while not extra_logs_pruned and len(logs_to_evaluate) > 0:
    if get_log_time(logs_to_evaluate[0]) < cutoff_time:
      logs_to_evaluate.pop(0)
    else: # We're done.  All logs left should be in the proper time range
      extra_logs_pruned = True
    
  # Return list of logs in desired time range
  return logs_to_evaluate

# Get datetime corresponding to log line specified by parsing log contents
def get_log_time(log_line):

  # Assume log line looks something like this:
  # 2017-03-01T17:08:17.086-0500: 6689.759: [Full GC (Allocation Failure)...]
  #
  # Split log_line with space delimiter and grab the first segment
  split_line = log_line.split(' ')
  log_time = split_line.pop(0)

  # Return datetime built from timestamp substrings
  result_datetime = datetime(int(log_time[:4]), int(log_time[5:7]), int(log_time[8:10]), int(log_time[11:13]), int(log_time[14:16]), int(log_time[17:19]))
  return result_datetime

# Return boolean indicating whether it is safe to restart tomcat on this server
# This is done by checking how many instances appear up to AWS tomcat load
# balancer target groups (through awscli)
def tomcat_can_be_restarted():
  
  # Use AWS load balancer to determine how many tomcat instance are up.
  # Execute awscli command, parse for host status, and count healthy hosts
  aws_output = Popen(("aws elbv2 describe-target-health --target-group-arn "+load_balancer_arn).split(), stdout=PIPE)
  healthy_host_count = sum(1 for x in finditer(r'\"State\": \"healthy\"', aws_output.communicate()[0]))
  aws_output.stdout.close()

  # Return whether or not healthy_host_count is high enough to restart tomcat
  return healthy_host_count >= expected_tomcat_host_count 

def restart_tomcat():
  # Some assumptions are being made about tomcat paths here. Edit as needed.
  # Without more specifics about tomcat configs, this is just a guess on
  # how tomcat would be restarted.

  call(["./$CATALINA_HOME/bin/shutdown.sh;./$CATALINA_HOME/bin/startup.sh"])
  return True

# "logs" is a set of logs we're checking for GC issues.
#
# If GC memory reclamation becomes too small, or if too much time is spent
# collecting garbage, send alerts and trigger tomcat restarts.
def eval_logs_for_full_gc_issues(logs):
  # Assume: All is well to start,
  #         Zero low memory states post-collection
  #         Zero time spend on full GCs
  gc_okay = True
  low_memory_count = 0
  total_gc_time = 0.0

  # Evaluate each line individually and check for GC problems
  for x in logs:
    split_line = x.split(' ')
    split_line.pop(0) # Drop timestamp
    split_line.pop(0) # Drop time
    log_data = " ".join(split_line)

    # If log indicates a full GC, examine it carefully
    if log_data[0:8] == "[Full GC": # Full GC detected!
      resplit_line = log_data.split(")")
      full_gc_cause = resplit_line[0] # Cause info from log line
      mem_metrics = resplit_line[1] # GC memory stats from log line
      full_gc_time = resplit_line[2].split(" ")[1] # Time spent on GC

      # Calculate amount of memory reclaimed by GC
      premem = mem_metrics.split("->")[0]
      postmem = mem_metrics.split("->")[1].split("(")[0]
      nonfreed_mem = float(postmem[:len(postmem)-1])/float(premem[:len(premem)-1])
 
      # If insufficient memory was reclaimed by GC, count it
      if (1 - nonfreed_mem) < full_gc_low_mem_threshold:
        low_memory_count += 1
      total_gc_time += float(full_gc_time)

  # Finally, check GC time and memory stat totals and alert if necessary
  if low_memory_count >= max_low_mem_full_GCs:
    print "Full GCs reclaiming too little memory.  Alerting @", datetime.now()
    if tomcat_can_be_restarted():
      restart_tomcat()
      send_gc_alert("Full GCs recovering insufficient memory - tomcat restarted") 
  if (float(full_gc_time) > (max_full_GC_time * log_time_period)):
    print "Total GC time over last monitoring period exceeds limits.  Alerting @", datetime.now()
    if tomcat_can_be_restarted():
      restart_tomcat()
      send_gc_alert("Full GC duration exceeds set limits - tomcat restarted")


def send_gc_alert(alert_text):
  print alert_text
  
  # Build an email to send to a paging system and wake someone up
  message = MIMEText(alert_text+" / Alerting @ "+str(datetime.now()))
  message["Subject"] = "Application alert - GC triggered tomcat restart!"
  message["From"] = "monitor@notarealdomain987.com"
  message["To"] = "sev2.app.alert.pagerduty@notarealdomain987.com"
  p = Popen(["/usr/sbin/sendmail", "-t", "-oi"], stdin=PIPE)
  p.communicate(message.as_string())

# Take list of logs and break them down into a list of JSON objects
# to send to New Relic Insights
#
# Includes some duplication of stuff in above evaluation
# Could be cleaned up more, timestamps are treated like text,
# plenty of streamlining ... TBD
def export_gc_logs_to_new_relic(logs):
  
  # Parse logs one at a time
  for x in logs:
    split_line = x.split(' ')
    ts = split_line.pop(0) # Get timestamp
    t = split_line.pop(0) # Get time
    secs = split_line.pop() # Get "secs]", asssumed end of every gc log line
    dur = split_line.pop() # Get GC log duration (in seconds)
    data = " ".join(split_line) # Reassemble log data

    # Set default GC log details for NR export
    gc_event_type = "N/A"
    cause = "N/A"
    action = "N/A"
    collector = "N/A"

    # Is this a FULL GC log?
    # Assume format: 
    # $DATETIME: TIME: [Full GC ($CAUSE), SOMEDATA, $DURATION secs]

    if data[0:8] == "[Full GC": # If yes...
      gc_event_type = "Full GC"
      split_line.pop(0) # [Full
      split_line.pop(0) # GC
      get_cause = " ".join(split_line)
      cause = get_cause[get_cause.find("(")+1:get_cause.find(")")]

      build_new_relic_json(gc_event_type, cause, action, collector, dur, ts)
    
    # Is this a GC PAUSE log?
    # Assume format:
    # $DATETIME: TIME: [GC pause, ($ACTION) ($COLLECTOR) ($CAUSE), $DURATION secs]
    
    elif data[0:9] == "[GC pause":
      gc_event_type = "GC pause"
      split_line.pop(0) # [GC
      split_line.pop(0) # pause,
      joined_line = " ".join(split_line)
      action = joined_line[joined_line.find("(")+1:joined_line.find(")")]
      split_line = data.split(")")
      split_line.pop(0) # get rid of action, get access to collector
      joined_line = ")".join(split_line)
      collector = joined_line[joined_line.find("(")+1:joined_line.find(")")]
      split_line = joined_line.split(")")
      split_line.pop(0) # get rid of collector, get access to cause
      joined_line = ")".join(split_line)
      if joined_line.find("(") != -1:
        cause = joined_line[joined_line.find("(")+1:joined_line.find(")")]

      build_new_relic_json(gc_event_type, cause, action, collector, dur, ts)

    # Is this some other interesting GC log?
    # Assume format:
    # $DATETIME: TIME: [GC $EVENT SOMEDATA, $DURATION secs]

    elif data[0:4] == "[GC ":

      if data[4:35] == "concurrent-root-region-scan-end":
        gc_event_type = data[1:35]
      elif data[4:23] == "concurrent-mark-end":
        gc_event_type = data[1:23]
      elif data[4:26] == "concurrent-cleanup-end":
        gc_event_type = data[1:26]
      elif data[4:11] == "Cleanup":
        gc_event_type = data[1:11]
      elif data[4:10] == "remark":
        gc_event_type = data[1:10]
      elif data[4:12] == "ref-proc":
        gc_event_type = data[1:12]

      if gc_event_type != "N/A":
        build_new_relic_json(gc_event_type, cause, action, collector, dur, ts)

# Print New Relic Insights JSON to standard out
# Could be a lot better, but I ran out of time
def build_new_relic_json(gc_event_type, cause, action, collector, dur, time):  
  print "["
  print "  {"
  print "    \"gcEventType\":\""+gc_event_type+"\","
  print "    \"gcCause\":\""+cause+"\","
  print "    \"gcAction\":\""+action+"\","
  print "    \"gcCollector\":\""+collector+"\","
  print "    \"gcDurationInSeconds\":"+dur+","
  print "    \"gcTimestamp\":\""+time+"\""
  print "  }"
  print "]"


##################
# Run the script #
##################

logs = get_last_five_minutes_of_gc_logs(log_file_location())
eval_logs_for_full_gc_issues(logs)
export_gc_logs_to_new_relic(logs)
